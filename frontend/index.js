const searchForm = document.querySelector(".search__form");
const resultContainer = document.querySelector(".search__result");

searchForm.addEventListener("submit", function (e) {
    e.preventDefault();

    // стереть детей

    const value = document.querySelector(".search__input").value;

    fetch('http://127.0.0.1:8000/track_info/' + value)

        .then((response) => {
            return response.json();
        })
        .then((data) => {
            data.lyrics.forEach(element => {
                let string = document.createElement("p");
                string.classList.add("search__text");
                string.textContent = element;
                resultContainer.appendChild(string);
            });
        });
});

