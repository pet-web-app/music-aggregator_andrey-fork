# Find lyrics 

## What does it do

Enter the song name in the web interface and after the magic you will be able to read it`s lyrics

![Interface](/.doc/interface screenshot.jpg) 

## Project`s structure

It`s a simple web application which has:
- [ ]  HTML/JavaScrip/CSS frontend
- [ ]  Python backened


## Project`s aims
- [ ]  Get familiar with python's asyncio module
- [ ]  Get familiar with simple web app deployment

## 
## Deployment manual

In order to deploy app you have to:
- [ ]  add GENIUS_TOKEN in environmet variables (read [here](https://genius.com/signup_or_login) how to fetch it)
- [ ]  run command 
```docker-compose -f "docker-compose.yaml" up -d --build```


## Status

The project is out of date cause GENIUS changed HTML`source code where the texts were taken from
