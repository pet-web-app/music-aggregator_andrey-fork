from fastapi import FastAPI
from genius import Genius
from fastapi.middleware.cors import CORSMiddleware

app = FastAPI()

origins = ["*"]
app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)


@app.get('/track_info/{track_name}')
async def get_song_lyrics(track_name):
    result = {}
    async with Genius() as genius:
        result["lyrics"] = await genius.get_lyrics_by_trackname(track_name)
    return result