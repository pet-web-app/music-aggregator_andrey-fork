import aiohttp
import asyncio
from bs4 import BeautifulSoup
import logging
import os

logger = logging.getLogger()


class Genius():
    def __init__(self) -> None:
        self.token = os.getenv("GENIUS_TOKEN")

    async def __aenter__(self):
        self.session = aiohttp.ClientSession()
        return self

    async def __aexit__(self, exc_type, exc_val, exc_tb):
        await self.session.close()

    async def search(self, query):
        logger.info(f"Запуск search для {query}")
        headers = {'Authorization': f'Bearer {self.token}'}
        params = {'q': query}
        async with self.session.get('https://api.genius.com/search', headers=headers, params=params) as response:
            result = await response.json()
        return result

    async def songs(self, id):
        logger.info(f"Запуск songs для {id}")
        headers = {'Authorization': f'Bearer {self.token}'}
        async with self.session.get(f'https://api.genius.com/songs/{id}', headers=headers) as response:
            result = await response.json()
        return result

    async def get_lyrics_by_trackname(self, track_name):
        search_result = await self.search(track_name)
        try:
            id = search_result['response']['hits'][0]['result']['id']
            logger.info(f"{track_name} имеет id {id}")
            songs_result = await self.songs(id)
            lyrics_url = songs_result['response']['song']['url']
            headers = {'Authorization': f'Bearer {self.token}'}
            async with self.session.get(lyrics_url, headers=headers) as response:
                raw_html = await response.text()
            lyrics = self.get_lyrics_from_html(raw_html)
        except IndexError:
            lyrics = ""
        return lyrics

    def get_lyrics_from_html(self, html):
        html = BeautifulSoup(html, 'html.parser')
        lyrics_html = html.find(class_='Lyrics__Container-sc-1ynbvzw-6 lgZgEN')
        lyrics = lyrics_html.get_text("\n").split("\n")
        return lyrics


async def main():
    track_list = ["slow thai", "Faint", "Oxxymiron скучно жить", "21 pilots", "the beatless", "ЛСП"]
    async with Genius() as genius:
        tasks = [asyncio.create_task(genius.get_lyrics_by_trackname(track)) for track in track_list]
        results = [await t for t in tasks]


if __name__ == "__main__":
    loop = asyncio.get_event_loop()
    loop.run_until_complete(main())
